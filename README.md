# SWINDLE-SHEETS (P1)

A backend API implementation to be used in trucking industry software.   
   
## Technologies Used

* Javalin 3.13.3
* Jackson 2.10.3
* Hibernate 5.4.30
* Microsoft SQL Server
* Azure-hosted Ubuntu VM
  - Jenkins, Docker

## OOA and ERD

***Truckers*** are paid for completing ***assignments***. They can be paid by
the hour, by the ***weight of the cargo*** they are transporting, by the ***miles that
they travel***. Individual assignments (***loads***) consist of stopping at ***multiple
different locations (route points)***, where they will either drop off or pick
up ***commodities***. Commodities sometimes require extra licenses to transport
or special equipment if they are ***hazardous in some way***.   
   
Users (truckers) - have user login/account details and also a historical record of loads they've completed   
Loads (individual jobs) - have a list of route points that were visited and a list of commodities that were transported   
Route Points - can either ship or receive cargo, and have location information   
Commodities (cargo) - tracked by weight and can have a placard that specifies hazards

![alt ERD](assets/erd.png "Entity Relationship Diagram")

## Features

* Registering a new account with name, email, and password.
* Logging in with email and password.
* Providing an access token upon successful login.
* Password salting and hashing for added security.
* Role-based authorization for specific features.
* Creating a Load history, tracking stops in a load, and a location and commodities for stops.
* Adding new Route Points (locations). (Admin only)
* Adding new commodities. (Admin only)
* Querying commodity and route point data by various fields.

## ENDPOINTS

### /authorize

> **POST /authorize**
> - Content-Type: application/x-www-form-urlencoded
>   - username and password form parameters required
> - Returns token string in body used for authentication or 401

### /users

> **GET /users/{user-id}**
> - Returns user resource or 404

> **GET /users?employer={employer-name}**
> - Allows for querying all users or by their given employer

> **POST /users**
> - Content-Type: application/x-www-form-urlencoded
>   - username, email, and password form parameters required
>   - employer form parameter optional
> - Creates new user resource or returns 409 upon conflicting username/email

> **PUT /users/{user-id}**
> - Requires authentication as admin or user resource owner
> - Content-Type: application/x-www-form-urlencoded
>   - Password form parameter will update the user's password
>   - Any combination of username, email, and employer will update the user resource
> - Returns 409 upon conflicting username/email updates

> **DELETE /users/{user-id}**
> - Requires authentication as admin or user resource owner
> - Deletes user resource or returns 404

### /loads

> **GET /loads/{load-id}**
> - Returns load resource or 404

> **GET /loads/?user={user-id}**
> - Allows for querying all loads or loads of a specific user

> **POST /loads**
> - Requires authentication
> - Content-Type: application/json
> - Creates new load resource

> **PUT /loads/{load-id}**
> - Requires authentication as admin or user resource owner
> - Content-Type: application/json
> - Updates load resource or returns 404

> **DELETE /loads/{load-id}**
> - Requires authentication as admin or user resource owner
> - Deletes load resource or returns 404

### /commodities

> **GET /commodities/{commodity-id}**
> - Returns commodity resource or 404

> **GET /commodities?placard={placard-name}**
> - Allows for querying all commodities or by their placard

> **POST /commodities**
> - Requires authentication as admin
> - Content-Type: application/json
> - Creates new commodity resource or returns 409 if an identical commodity  exists

> **PUT /commodities/{commodity-id}**
> - Requires authentication as admin
> - Content-Type: application/json
> - Updates commodity resource or returns 409

> **DELETE /commodities/{commodity-id}**
> - Requires authentication as admin
> - Deletes commodity resource or returns 404

### /route-points

> **GET /route-points/{route-point-id}**
> - Returns route point resource or 404

> **GET /route-points?type={type}&city={city}&state={state}&zip={zip}**
> - Allows for querying all route points or by given parameters
>   - type: SHIPPER, RECEIVER, TERMINAL, TRUCK_STOP
>   - city
>   - state (abbreviated)
>   - zip

> **POST /route-points**
> - Requires authentication as admin
> - Content-Type: application/json
> - Creates new route point resource

> **PUT /route-points/{route-point-id}**
> - Requires authentication as admin
> - Content-Type: application/json
> - Updates route point resource

> **DELETE /route-points/{route-point-id}**
> - Requires authentication as admin
> - Deletes route point resource

![alt Mock](assets/fake_fe.png "Fake")

## Contributors

* Steffen Moseley (@scm16)
