FROM java:8
COPY build/libs/trucker-aid-0.0.1.jar .
EXPOSE 80
CMD java -jar trucker-aid-0.0.1.jar
