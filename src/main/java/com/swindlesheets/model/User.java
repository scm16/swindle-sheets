package com.swindlesheets.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "user_account")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true, length = 36)
    private String username;

    @Column(unique = true, length = 254)
    private String email;

    @Column(length = 46)
    private String employer;

    public User() {}

    public User(int id) {
        this.id = id;
    }

    public User(String username, String email, String employer) {
        this.username = username;
        this.email = email;
        this.employer = employer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && username.equals(user.username) &&
                email.equals(user.email) && employer.equals(user.employer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, email, employer);
    }

}
