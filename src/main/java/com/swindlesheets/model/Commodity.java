package com.swindlesheets.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "commodity")
public class Commodity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 11)
    private Placard placard;

    @JsonIgnore
    @OneToMany(mappedBy = "commodity", cascade = CascadeType.ALL)
    private List<StopCommodity> stops = new ArrayList<>();

    public Commodity() {}

    public Commodity(String description, Placard placard) {
        this.description = description;
        this.placard = placard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Placard getPlacard() {
        return placard;
    }

    public void setPlacard(Placard placard) {
        this.placard = placard;
    }

    public List<StopCommodity> getStops() {
        return stops;
    }

    public void setStops(List<StopCommodity> stops) {
        this.stops = stops;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commodity commodity = (Commodity) o;
        return description.equals(commodity.description) &&
                placard.equals(commodity.placard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, placard);
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", placard=" + placard +
                '}';
    }

}
