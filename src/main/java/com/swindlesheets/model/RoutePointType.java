package com.swindlesheets.model;

public enum RoutePointType {

    SHIPPER,
    RECEIVER,
    TERMINAL,
    TRUCK_STOP

}
