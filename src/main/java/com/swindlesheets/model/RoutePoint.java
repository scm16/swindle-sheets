package com.swindlesheets.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "route_point")
public class RoutePoint implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private RoutePointType type;

    @Column(length = 46)
    private String name;

    @Column(length = 46)
    private String address;

    @Column(length = 50)
    private String city;

    @Column(length = 2)
    private String state;

    @Column(length = 10)
    private String zip;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RoutePointType getType() {
        return type;
    }

    public void setType(RoutePointType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoutePoint that = (RoutePoint) o;
        return id == that.id && type.equals(that.type) && name.equals(that.name) &&
                address.equals(that.address) && city.equals(that.city) &&
                state.equals(that.state) && zip.equals(that.zip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, name, address, city, state, zip);
    }

}
