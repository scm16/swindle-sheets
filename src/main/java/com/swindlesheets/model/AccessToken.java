package com.swindlesheets.model;

public class AccessToken {

    private int userId;
    private Role role;
    private long timestamp;

    public AccessToken(int userId, Role role, long timestamp) {
        this.userId = userId;
        this.role = role;
        this.timestamp = timestamp;
    }

    public String toTokenString() {
        return timestamp + ":" + userId + ":" + false;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
