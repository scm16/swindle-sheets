package com.swindlesheets.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.*;

@Entity
@Table(name = "stop")
public class Stop implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "load_id")
    private Load load;

    @OneToOne
    @JoinColumn(name = "route_point_id")
    private RoutePoint routePoint;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "stop", cascade = CascadeType.ALL)
    private List<StopCommodity> commodities = new ArrayList<>();

    private Date date;
    private int odometer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Load getLoad() {
        return load;
    }

    public void setLoad(Load load) {
        this.load = load;
    }

    public RoutePoint getRoutePoint() {
        return routePoint;
    }

    public void setRoutePoint(RoutePoint routePoint) {
        this.routePoint = routePoint;
    }

    public List<StopCommodity> getCommodities() {
        return commodities;
    }

    public void setCommodities(List<StopCommodity> commodities) {
        this.commodities = commodities;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public void addCommodity(Commodity commodity) {
        StopCommodity stopCommodity = new StopCommodity(this, commodity);
        commodities.add(stopCommodity);
        commodity.getStops().add(stopCommodity);
    }

    public void removeCommodity(Commodity commodity) {
        Iterator<StopCommodity> iterator = commodities.iterator();
        while (iterator.hasNext()) {
            StopCommodity stopCommodity = iterator.next();
            if (stopCommodity.getStop().equals(this) &&
                    stopCommodity.getCommodity().equals(commodity)) {
                iterator.remove();
                stopCommodity.getCommodity().getStops().remove(stopCommodity);
                stopCommodity.setStop(null);
                stopCommodity.setCommodity(null);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stop stop = (Stop) o;
        return Objects.equals(load, stop.load) &&
                Objects.equals(routePoint, stop.routePoint);
    }

    @Override
    public int hashCode() {
        return Objects.hash(load, routePoint);
    }

    @Override
    public String toString() {
        return "Stop{" +
                "id=" + id +
                ", routePoint=" + routePoint +
                ", commodities=" + commodities +
                ", date=" + date +
                ", odometer=" + odometer +
                '}';
    }

}
