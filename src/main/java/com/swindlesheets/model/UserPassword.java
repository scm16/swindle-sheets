package com.swindlesheets.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "user_password")
public class UserPassword {

    @Id
    @Column(name = "user_id")
    private int id;

    @OneToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @MapsId
    private User user;

    private byte[] salt, hash;

    public UserPassword() {}

    public UserPassword(User user, byte[] salt, byte[] hash) {
        this.user = user;
        this.salt = salt;
        this.hash = hash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPassword that = (UserPassword) o;
        return id == that.id && Objects.equals(user, that.user) &&
                Arrays.equals(salt, that.salt) && Arrays.equals(hash, that.hash);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, user);
        result = 31 * result + Arrays.hashCode(salt);
        result = 31 * result + Arrays.hashCode(hash);
        return result;
    }

}
