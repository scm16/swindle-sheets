package com.swindlesheets.model;

public enum Placard {
    NONE,
    EXPLOSIVE,
    GAS,
    FLAMMABLE,
    TOXIC,
    RADIOACTIVE,
    CORROSIVE
}
