package com.swindlesheets.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class StopCommodityId implements Serializable {

    @Column(name = "stop_id")
    private int stopId;

    @Column(name = "commodity_id")
    private int commodityId;

    public StopCommodityId() {}

    public StopCommodityId(int stopId, int commodityId) {
        this.stopId = stopId;
        this.commodityId = commodityId;
    }

    public int getStopId() {
        return stopId;
    }

    public void setStopId(int stopId) {
        this.stopId = stopId;
    }

    public int getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(int commodityId) {
        this.commodityId = commodityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StopCommodityId that = (StopCommodityId) o;
        return stopId == that.stopId && commodityId == that.commodityId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stopId, commodityId);
    }
}
