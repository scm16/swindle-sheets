package com.swindlesheets.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "stop_commodity")
public class StopCommodity implements Serializable {

    @JsonIgnore
    @EmbeddedId
    private StopCommodityId id = new StopCommodityId();

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("stopId")
    private Stop stop;

    @ManyToOne
    @MapsId("commodityId")
    private Commodity commodity;

    @Enumerated(EnumType.STRING)
    @Column(length = 6)
    private StopCommodityAction action;

    private int weight;

    public StopCommodity() {}

    public StopCommodity(Stop stop, Commodity commodity) {
        this.stop = stop;
        this.commodity = commodity;
        this.id = new StopCommodityId(stop.getId(), commodity.getId());
    }

    public StopCommodityId getId() {
        return id;
    }

    public void setId(StopCommodityId id) {
        this.id = id;
    }

    public Stop getStop() {
        return stop;
    }

    public void setStop(Stop stop) {
        this.stop = stop;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public StopCommodityAction getAction() {
        return action;
    }

    public void setAction(StopCommodityAction action) {
        this.action = action;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StopCommodity that = (StopCommodity) o;
        return Objects.equals(stop, that.stop) &&
                Objects.equals(commodity, that.commodity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stop, commodity);
    }

    @Override
    public String toString() {
        return "StopCommodity{" +
                "commodity=" + commodity +
                ", action=" + action +
                ", weight=" + weight +
                '}';
    }

}
