package com.swindlesheets;

import com.swindlesheets.util.Sessions;
import org.hibernate.Session;

public class Main {

    private static final int PORT = 80;

    public static void main(String[] args) {
        JavalinApplication app = new JavalinApplication();
        app.start(PORT);
        //
        try (Session s = Sessions.getSession()) {}
        //
    }

}
