package com.swindlesheets.dao;

import com.swindlesheets.util.Sessions;
import io.javalin.http.NotFoundResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.Commodity;

import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommodityDAO implements ICommodityDAO {

    private final Logger logger = LoggerFactory.getLogger(CommodityDAO.class);

    @Override
    public Commodity getCommodity(int commodityId) {
        try (Session session = Sessions.getSession()) {
            return session.get(Commodity.class, commodityId);
        }
    }

    @Override
    public List<Commodity> queryCommodities(Map<String, String> parameters) {
        try (Session session = Sessions.getSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Commodity> query = builder.createQuery(Commodity.class);
            Root<Commodity> root = query.from(Commodity.class);
            query.select(root);
            if (parameters.isEmpty()) {
                return session.createQuery(query).list();
            }
            List<Predicate> predicates = new ArrayList<>();
            if (parameters.containsKey("description")) {
                predicates.add(builder.like(
                        builder.lower(root.get("description")),
                        "%" + parameters.get("description").toLowerCase() + "%"));
            }
            if (parameters.containsKey("placard")) {
                predicates.add(builder.equal(root.get("placard"), parameters.get("placard")));
            }
            query.where(predicates.toArray(new Predicate[]{}));
            return session.createQuery(query).list();
        }
    }

    @Override
    public void saveNewCommodity(Commodity commodity) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(commodity);
            transaction.commit();
        }
    }

    @Override
    public void updateCommodity(Commodity commodity) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(commodity);
            transaction.commit();
        }
    }

    @Override
    public void deleteCommodity(int commodityId) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            Commodity commodity = session.load(Commodity.class, commodityId);
            session.delete(commodity);
            transaction.commit();
        } catch (EntityNotFoundException exception) {
            throw new NotFoundResponse();
        }
    }

}
