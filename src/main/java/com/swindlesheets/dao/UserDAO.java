package com.swindlesheets.dao;

import com.swindlesheets.model.UserPassword;
import com.swindlesheets.util.Sessions;
import io.javalin.http.ConflictResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.User;

import java.util.List;

public class UserDAO implements IUserDAO {

    private final Logger logger = LoggerFactory.getLogger(UserDAO.class);

    @Override
    public User getUser(int id) {
        try (Session session = Sessions.getSession()) {
            return session.get(User.class, id);
        }
    }

    @Override
    public User getUser(String username) {
        try (Session session = Sessions.getSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            return query.uniqueResult();
        }
    }

    @Override
    public UserPassword getUserPassword(User user) {
        try (Session session = Sessions.getSession()) {
            return session.get(UserPassword.class, user.getId());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getAllUsers() {
        try (Session session = Sessions.getSession()) {
            return session.createQuery("from User").getResultList();
        }
    }

    @Override
    public void saveNewUser(User user, UserPassword password) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(user);
            session.save(password);
            transaction.commit();
        } catch (ConstraintViolationException exception) {
            throw new ConflictResponse();
        }
    }

    @Override
    public void updateUser(User user) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
        } catch (ConstraintViolationException exception) {
            throw new ConflictResponse();
        }
    }

    @Override
    public void updateUserPassword(UserPassword password) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(password);
            transaction.commit();
        }
    }

    @Override
    public void deleteUser(int id) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            User user = session.load(User.class, id);
            session.delete(user);
            transaction.commit();
        }
    }

}
