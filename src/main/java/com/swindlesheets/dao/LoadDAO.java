package com.swindlesheets.dao;

import com.swindlesheets.model.Load;
import com.swindlesheets.model.Stop;
import com.swindlesheets.model.User;
import com.swindlesheets.util.Sessions;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class LoadDAO implements ILoadDAO {

    private final Logger logger = LoggerFactory.getLogger(LoadDAO.class);

    @Override
    public Load getLoad(int loadId) {
        try (Session session = Sessions.getSession()) {
            return session.get(Load.class, loadId);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Load> getUserLoads(User user) {
        try (Session session = Sessions.getSession()) {
            Query<Load> query = session.createQuery("from Load where user = :user");
            query.setParameter("user", user);
            return query.getResultList();
        }
    }

    @Override
    public void saveNewLoad(Load load) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(load);
            transaction.commit();
        }
    }

    @Override
    public void updateLoad(Load load) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(load);
            transaction.commit();
        }
    }

    @Override
    public void deleteLoad(Load load) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(load);
            transaction.commit();
        }
    }

    public Stop getStop(int stopId) {
        try (Session session = Sessions.getSession()) {
            return session.get(Stop.class, stopId);
        }
    }

    public void saveNewStop(Stop stop) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(stop);
            transaction.commit();
        }
    }

    public void deleteStop(Stop stop) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(stop);
            transaction.commit();
        }
    }

}
