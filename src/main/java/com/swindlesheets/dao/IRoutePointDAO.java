package com.swindlesheets.dao;

import com.swindlesheets.model.RoutePoint;

import java.util.List;
import java.util.Map;

public interface IRoutePointDAO {

    RoutePoint getRoutePoint(int id);
    List<RoutePoint> queryRoutePoints(Map<String, String> parameters);
    void saveNewRoutePoint(RoutePoint routePoint);
    void updateRoutePoint(RoutePoint routePoint);
    void deleteRoutePoint(int routePointId);

}
