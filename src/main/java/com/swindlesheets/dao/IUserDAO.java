package com.swindlesheets.dao;

import com.swindlesheets.model.UserPassword;
import com.swindlesheets.model.User;

import java.util.List;

public interface IUserDAO {

    User getUser(int userId);
    User getUser(String username);
    UserPassword getUserPassword(User user);
    List<User> getAllUsers();
    void saveNewUser(User user, UserPassword password);
    void updateUser(User user);
    void updateUserPassword(UserPassword password);
    void deleteUser(int userId);

}
