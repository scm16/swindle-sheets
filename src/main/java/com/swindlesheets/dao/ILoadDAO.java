package com.swindlesheets.dao;

import com.swindlesheets.model.Stop;
import com.swindlesheets.model.Load;
import com.swindlesheets.model.User;

import java.util.List;

public interface ILoadDAO {

    Load getLoad(int loadId);
    List<Load> getUserLoads(User user);
    void saveNewLoad(Load load);
    void updateLoad(Load load);
    void deleteLoad(Load load);
    Stop getStop(int stopId);
    void saveNewStop(Stop stop);
    void deleteStop(Stop stop);

}
