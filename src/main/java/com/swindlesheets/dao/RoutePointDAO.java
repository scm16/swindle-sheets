package com.swindlesheets.dao;

import com.swindlesheets.model.RoutePointType;
import com.swindlesheets.util.Sessions;
import io.javalin.http.NotFoundResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.RoutePoint;

import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoutePointDAO implements IRoutePointDAO {

    private final Logger logger = LoggerFactory.getLogger(RoutePointDAO.class);

    @Override
    public RoutePoint getRoutePoint(int id) {
        try (Session session = Sessions.getSession()) {
            return session.get(RoutePoint.class, id);
        }
    }

    @Override
    public List<RoutePoint> queryRoutePoints(Map<String, String> parameters) {
        try (Session session = Sessions.getSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<RoutePoint> query = builder.createQuery(RoutePoint.class);
            Root<RoutePoint> root = query.from(RoutePoint.class);
            query.select(root);
            if (parameters.isEmpty()) {
                return session.createQuery(query).list();
            }
            List<Predicate> predicates = new ArrayList<>();
            if (parameters.containsKey("type")) {
                predicates.add(builder.equal(root.get("type"), RoutePointType.valueOf(parameters.get("type"))));
            }
            if (parameters.containsKey("city")) {
                predicates.add(builder.equal(root.get("city"), parameters.get("city")));
            }
            if (parameters.containsKey("state")) {
                predicates.add(builder.equal(root.get("state"), parameters.get("state")));
            }
            if (parameters.containsKey("zip")) {
                predicates.add(builder.equal(root.get("zip"), parameters.get("zip")));
            }
            query.where(predicates.toArray(new Predicate[]{}));
            return session.createQuery(query).list();
        }
    }

    @Override
    public void saveNewRoutePoint(RoutePoint routePoint) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(routePoint);
            transaction.commit();
        }
    }

    @Override
    public void updateRoutePoint(RoutePoint routePoint) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(routePoint);
            transaction.commit();
        }
    }

    @Override
    public void deleteRoutePoint(int id) {
        try (Session session = Sessions.getSession()) {
            Transaction transaction = session.beginTransaction();
            RoutePoint routePoint = session.load(RoutePoint.class, id);
            session.delete(routePoint);
            transaction.commit();
        } catch (EntityNotFoundException exception) {
            throw new NotFoundResponse();
        }
    }

}
