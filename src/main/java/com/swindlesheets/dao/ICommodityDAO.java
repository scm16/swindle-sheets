package com.swindlesheets.dao;

import com.swindlesheets.model.Commodity;

import java.util.List;
import java.util.Map;

public interface ICommodityDAO {

    Commodity getCommodity(int commodityId);
    List<Commodity> queryCommodities(Map<String, String> queryParameterMap);
    void saveNewCommodity(Commodity commodity);
    void updateCommodity(Commodity commodity);
    void deleteCommodity(int commodityId);

}
