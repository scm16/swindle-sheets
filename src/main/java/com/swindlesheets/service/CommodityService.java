package com.swindlesheets.service;

import io.javalin.http.BadRequestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.dao.ICommodityDAO;
import com.swindlesheets.model.Commodity;

import java.util.List;
import java.util.Map;

import static com.swindlesheets.util.DAOs.getCommodityDAO;

public class CommodityService {

    private final Logger logger = LoggerFactory.getLogger(CommodityService.class);
    private final ICommodityDAO commodityDAO = getCommodityDAO();

    public Commodity getCommodity(int commodityId) {
        return commodityDAO.getCommodity(commodityId);
    }

    public List<Commodity> queryCommodities(Map<String, String> queryMap) {
        if (queryMap.get("placard") != null) {
            validatePlacard(queryMap.get("placard"));
        }
        logger.trace("Valid parameters provided");
        return commodityDAO.queryCommodities(queryMap);
    }

    public void createNewCommodity(Commodity commodity) {
        validateDescription(commodity.getDescription());
        //validatePlacard(commodity.getPlacard());
        logger.trace("Valid parameters provided");
        commodityDAO.saveNewCommodity(commodity);
    }

    public void updateCommodity(Commodity commodity) {
        if (commodity.getDescription() != null) {
            validateDescription(commodity.getDescription());
        }
        if (commodity.getPlacard() != null) {
            //validatePlacard(commodity.getPlacard());
        }
        logger.trace("Valid parameters provided");
        commodityDAO.updateCommodity(commodity);
    }

    public void deleteCommodity(int commodityId) {
        commodityDAO.deleteCommodity(commodityId);
    }

    private void validateDescription(String description) {
        if (description.isEmpty() || description.length() > 40) {
            logger.warn("Invalid description parameter given");
            throw new BadRequestResponse("Invalid parameters: " +
                    "description must not be empty and have 40 or less characters");
        }
    }

    private void validatePlacard(String placard) {
        if (!placard.equals("NONE") && !placard.equals("EXPLOSIVE") && !placard.equals("GAS") &&
                !placard.equals("FLAMMABLE") && !placard.equals("TOXIC") &&
                !placard.equals("RADIOACTIVE") && !placard.equals("CORROSIVE")) {
            logger.warn("Invalid placard parameter given");
            throw new BadRequestResponse("Invalid parameters: nonexistent placard provided");
        }
    }

}
