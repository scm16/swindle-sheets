package com.swindlesheets.service;

import com.swindlesheets.model.Role;
import com.swindlesheets.model.UserPassword;
import io.javalin.http.InternalServerErrorResponse;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.User;
import com.swindlesheets.model.AccessToken;
import com.swindlesheets.dao.IUserDAO;
import com.swindlesheets.util.DAOs;
import com.swindlesheets.util.Passwords;

import java.util.Arrays;

public class AuthenticationService {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);
    private final IUserDAO userDAO = DAOs.getUserDAO();

    public AccessToken authenticateUser(String username, String password) {
        User user = userDAO.getUser(username);
        if (user == null) {
            throw new UnauthorizedResponse("Invalid credentials provided");
        }
        UserPassword userPassword = userDAO.getUserPassword(user);
        if (userPassword == null) {
            throw new InternalServerErrorResponse(); // User should always have 1-1 with password record
        }
        byte[] hashedPassword = Passwords.getPasswordHash(password, userPassword.getSalt());
        if (!Arrays.equals(hashedPassword, userPassword.getHash())) {
            throw new UnauthorizedResponse("Invalid credentials provided");
        }
        logger.info("Successful authentication of User#{}", user.getId());
        long timestamp = System.currentTimeMillis();
        return new AccessToken(user.getId(), Role.USER, timestamp);
    }

}
