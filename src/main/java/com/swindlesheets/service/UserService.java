package com.swindlesheets.service;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.dao.IUserDAO;
import com.swindlesheets.model.AccessToken;
import com.swindlesheets.model.User;
import com.swindlesheets.model.UserPassword;
import com.swindlesheets.util.DAOs;
import com.swindlesheets.util.Passwords;

import java.util.List;
import java.util.Map;

public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final IUserDAO userDAO = DAOs.getUserDAO();

    public User getUser(int userId) {
        return userDAO.getUser(userId);
    }

    public List<User> queryUsers(Map<String, String> queryMap) {
        if (queryMap.get("employer") != null) {
            validateEmployer(queryMap.get("employer"));
        }
        logger.trace("Valid parameters provided");
        return userDAO.getAllUsers();
    }

    public void createNewUser(String username, String email, String password, String employer) {
        validateUsername(username);
        validateEmail(email);
        validatePassword(password);
        validateEmployer(employer);
        logger.trace("Valid parameters provided");
        User user = new User(username, email, employer);
        byte[] salt = Passwords.getNewPasswordSalt();
        byte[] hash = Passwords.getPasswordHash(password, salt);
        UserPassword userPassword = new UserPassword(user, salt, hash);
        userDAO.saveNewUser(user, userPassword);
    }

    public void updateUser(AccessToken token, User user) {
        if (user.getUsername() != null) { validateUsername(user.getUsername()); }
        if (user.getEmail() != null) { validateEmail(user.getEmail()); }
        if (user.getEmployer() != null) { validateEmployer(user.getEmployer()); }
        logger.trace("Valid parameters provided");
        userDAO.updateUser(user);
    }

    public void updateUserPassword(AccessToken token, int userId, String password) {
        validatePassword(password);
        logger.trace("Valid password provided");
        User user = new User();
        user.setId(userId);
        byte[] salt = Passwords.getNewPasswordSalt();
        byte[] hash = Passwords.getPasswordHash(password, salt);
        userDAO.updateUserPassword(new UserPassword(user, salt, hash));
    }

    public void deleteUser(AccessToken token, int userId) {
        if (userId != token.getUserId()) {
            //TODO check for admin
            throw new UnauthorizedResponse();
        }
        userDAO.deleteUser(userId);
    }

    private void validateUsername(String username) {
        if (username.isEmpty() || username.length() > 40) {
            logger.warn("Invalid username parameter given");
            throw new BadRequestResponse("Invalid parameters: " +
                    "username must not be empty and be within 40 characters");
        }
    }

    private void validateEmail(String email) {
        if (email.isEmpty() || email.length() > 40) {
            logger.warn("Invalid email parameter given");
            throw new BadRequestResponse("Invalid parameters: " +
                    "email must not be empty and be within 40 characters");
        }
    }

    private void validatePassword(String password) {
        if (password.isEmpty()) {
            logger.warn("Invalid password parameter given");
            throw new BadRequestResponse("Invalid parameters: " +
                    "password must not be empty");
        }
    }

    private void validateEmployer(String employer) {
        if (employer.length() > 40) {
            logger.warn("Invalid employer parameter given");
            throw new BadRequestResponse("Invalid parameters: " +
                    "employer must be within 40 characters");
        }
    }

}
