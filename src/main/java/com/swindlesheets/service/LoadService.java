package com.swindlesheets.service;

import com.swindlesheets.model.*;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.dao.ILoadDAO;
import com.swindlesheets.dao.IUserDAO;
import com.swindlesheets.util.DAOs;

import java.util.Collections;
import java.util.List;

public class LoadService {

    private final Logger logger = LoggerFactory.getLogger(LoadService.class);
    private final IUserDAO userDAO = DAOs.getUserDAO();
    private final ILoadDAO loadDAO = DAOs.getLoadDAO();

    public Load getLoad(AccessToken token, int loadId) {
        Load load = loadDAO.getLoad(loadId);
        if (load == null) {
            throw new NotFoundResponse("Load#" + loadId + " was not found");
        }
        authorizeLoadAccess(token, load);
        return load;
    }

    public List<Load> getUserLoads(AccessToken token, int userId) {
        if (userId != token.getUserId()) {
            throw new ForbiddenResponse();
        }
        User user = userDAO.getUser(userId);
        if (user == null) {
            throw new NotFoundResponse();
        }
        List<Load> loads = loadDAO.getUserLoads(user);
        Collections.reverse(loads);
        return loads;
    }

    public void saveNewLoad(AccessToken token, Load load) {
        load.setUser(new User(token.getUserId()));
        validateNotes(load.getNotes());
        loadDAO.saveNewLoad(load);
    }

    public void updateLoad(AccessToken token, Load load) {
        if (load.getNotes() != null) {
            validateNotes(load.getNotes());
        }
        loadDAO.updateLoad(load);
    }

    public void deleteLoad(AccessToken token, int loadId) {
        Load load = getLoad(token, loadId);
        loadDAO.deleteLoad(load);
    }

    private void validateNotes(String notes) {
        if (notes.length() > 100) {
            logger.warn("Invalid notes parameter given");
            throw new BadRequestResponse("Invalid parameters: " +
                    "notes must be within 100 characters");
        }
    }

    //======================================================================

    public Stop getStop(AccessToken token, int loadId, int stopId) {
        //authorize access to load
        Stop stop = loadDAO.getStop(stopId);
        if (stop == null || stop.getLoad().getId() != loadId) {
            throw new NotFoundResponse("Stop#" + stopId + " was not found");
        }
        return stop;
    }

    public void saveNewStop(AccessToken token, int loadId, Stop stop) {
        Load load = getLoad(token, loadId);
        validateLoadStop(load, stop);
        validateStop(stop);
        for (StopCommodity stopCommodity : stop.getCommodities()) {
            stopCommodity.setStop(stop);
        }
        loadDAO.saveNewStop(stop);
    }

    public void deleteStop(AccessToken token, int loadId, int stopId) {
        Stop stop = getStop(token, loadId, stopId);
        loadDAO.deleteStop(stop);
    }

    private void authorizeLoadAccess(AccessToken token, Load load) {
        if (load.getUser().getId() != token.getUserId()) {
            throw new ForbiddenResponse("User#" + token.getUserId() +
                    " is not the resource owner of Load#" + load.getId());
        }
    }

    private void validateLoadStop(Load load, Stop stop) {
        if (load.getId() != stop.getLoad().getId()) {
            throw new BadRequestResponse();
        }
    }

    private void validateStop(Stop stop) {
        //TODO validate date and odometer, unloaded previously loaded, etc...
    }

}
