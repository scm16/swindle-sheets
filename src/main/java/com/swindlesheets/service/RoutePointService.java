package com.swindlesheets.service;

import com.swindlesheets.dao.IRoutePointDAO;
import com.swindlesheets.model.RoutePointType;
import io.javalin.http.BadRequestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.RoutePoint;

import java.util.List;
import java.util.Map;

import static com.swindlesheets.util.DAOs.getRoutePointDAO;

public class RoutePointService {

    private final Logger logger = LoggerFactory.getLogger(RoutePointService.class);
    private final IRoutePointDAO routePointDAO = getRoutePointDAO();

    public RoutePoint getRoutePoint(int routePointId) {
        return routePointDAO.getRoutePoint(routePointId);
    }

    public List<RoutePoint> queryRoutePoints(Map<String, String> queryMap) {
        if (queryMap.containsKey("type")) validateTypeString(queryMap.get("type"));
        if (queryMap.containsKey("city")) validateCity(queryMap.get("city"));
        if (queryMap.containsKey("state")) validateState(queryMap.get("state"));
        if (queryMap.containsKey("zip")) validateZip(queryMap.get("zip"));
        return routePointDAO.queryRoutePoints(queryMap);
    }

    public void createNewRoutePoint(RoutePoint routePoint) {
        //validateType(routePoint.getType());
        validateName(routePoint.getName());
        validateAddress(routePoint.getAddress());
        validateCity(routePoint.getCity());
        validateState(routePoint.getState());
        validateZip(routePoint.getZip());
        logger.trace("Valid parameters provided");
        routePointDAO.saveNewRoutePoint(routePoint);
    }

    public void updateRoutePoint(RoutePoint routePoint) {
        //if (routePoint.getType() != null) { validateType(routePoint.getType()); }
        if (routePoint.getName() != null) { validateName(routePoint.getName()); }
        if (routePoint.getAddress() != null) { validateAddress(routePoint.getAddress()); }
        if (routePoint.getCity() != null) { validateCity(routePoint.getCity()); }
        if (routePoint.getState() != null) { validateState(routePoint.getState()); }
        if (routePoint.getZip() != null) { validateZip(routePoint.getZip()); }
        logger.trace("Valid parameters provided");
        routePointDAO.updateRoutePoint(routePoint);
    }

    public void deleteRoutePoint(int routePointId) {
        routePointDAO.deleteRoutePoint(routePointId);
    }

    private void validateTypeString(String typeString) {
        boolean valid = false;
        for (RoutePointType type : RoutePointType.values()) {
            if (type.name().equals(typeString)) {
                valid = true;
                break;
            }
        }
        if (!valid) {
            logger.warn("Invalid type parameter given");
            throw new BadRequestResponse();
        }
    }

    private void validateName(String name) {
        if (name.isEmpty() || name.length() > 40) {
            logger.warn("Invalid name parameter given");
            throw new BadRequestResponse("Invalid parameters: name must not be empty and be within 40 characters");
        }
    }

    private void validateAddress(String address) {
        if (address.isEmpty() || address.length() > 40) {
            logger.warn("Invalid address parameter given");
            throw new BadRequestResponse("Invalid parameters: address must not be empty and be within 40 characters");
        }
    }

    private void validateCity(String city) {
        if (city.isEmpty() || city.length() > 20) {
            logger.warn("Invalid city parameter given");
            throw new BadRequestResponse("Invalid parameters: city must not be empty and be within 20 characters");
        }
    }

    private void validateState(String state) {
        if (state.length() != 2) {
            logger.warn("Invalid state parameter given");
            throw new BadRequestResponse("Invalid parameters: state must be represented as abbreviation");
        }
    }

    private void validateZip(String zip) {
        if (!zip.matches("^[0-9]{5}(?:-[0-9]{4})?$")) {
            logger.warn("Invalid zip parameter given");
            throw new BadRequestResponse("Invalid parameters: zip must be represented as 5 or 9 digits");
        }
    }

}
