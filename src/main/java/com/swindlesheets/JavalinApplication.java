package com.swindlesheets;

import com.swindlesheets.controller.*;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApplication {

    private AuthenticationController authenticationController = new AuthenticationController();
    private UserController userController = new UserController();
    private LoadController loadController = new LoadController();
    private RoutePointController routePointController = new RoutePointController();
    private CommodityController commodityController = new CommodityController();

    private Javalin application;
    
    public JavalinApplication() {
        application = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(() -> {
            path("/authorize", () -> post(authenticationController::handlePostAuthorize));
            path("/users", () -> {
                get(userController::handleGetUsers);
                post(userController::handlePostUsers);
                path("/:userId", () -> {
                    get(userController::handleGetUser);
                    put(userController::handlePutUser);
                    delete(userController::handleDeleteUser);
                    path("loads", () -> {
                        get(loadController::handleGetUserLoads);
                    });
                });
            });
            path("/loads", () -> {
                get(loadController::handleGetLoads);
                post(loadController::handlePostLoads);
                path("/:loadId", () -> {
                    get(loadController::handleGetLoad);
                    put(loadController::handlePutLoad);
                    delete(loadController::handleDeleteLoad);
                    path("/stops", () -> {
                        post(loadController::handlePostStop);
                        path("/:stopId", () -> {
                            delete(loadController::handleDeleteStop);
                        });
                    });
                });
            });
            path("/route-points", () -> {
                get(routePointController::handleGetRoutePoints);
                post(routePointController::handlePostRoutePoints);
                path("/:routePointId", () -> {
                    get(routePointController::handleGetRoutePoint);
                    put(routePointController::handlePutRoutePoint);
                    delete(routePointController::handleDeleteRoutePoint);
                });
            });
            path("/commodities", () -> {
                get(commodityController::handleGetCommodities);
                post(commodityController::handlePostCommodities);
                path("/:commodityId", () -> {
                    get(commodityController::handleGetCommodity);
                    put(commodityController::handlePutCommodity);
                    delete(commodityController::handleDeleteCommodity);
                });
            });
        });
    }

    public void start(int port) { application.start(port); }

    public void stop() { application.stop(); }

}
