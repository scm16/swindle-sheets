package com.swindlesheets.util;

import com.swindlesheets.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public final class Sessions {

    private static SessionFactory sessionFactory;

    private static Properties getProperties() {
        Properties properties = new Properties();
        properties.put(Environment.URL, System.getenv("DB_URL"));
        properties.put(Environment.USER, System.getenv("DB_USER"));
        properties.put(Environment.PASS, System.getenv("DB_PASS"));
        properties.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
        properties.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");
        properties.put(Environment.HBM2DDL_AUTO, "validate");
        properties.put(Environment.SHOW_SQL, "true");
        return properties;
    }

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration config = new Configuration();
            config.setProperties(getProperties());
            config.addAnnotatedClass(Commodity.class);
            config.addAnnotatedClass(Load.class);
            config.addAnnotatedClass(RoutePoint.class);
            config.addAnnotatedClass(Stop.class);
            config.addAnnotatedClass(StopCommodity.class);
            config.addAnnotatedClass(StopCommodityId.class);
            config.addAnnotatedClass(User.class);
            config.addAnnotatedClass(UserPassword.class);
            config.addAnnotatedClass(UserRole.class);
            sessionFactory = config.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession() {
        return getSessionFactory().openSession();
    }

}
