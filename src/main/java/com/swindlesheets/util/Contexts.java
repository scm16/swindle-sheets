package com.swindlesheets.util;

import com.swindlesheets.model.AccessToken;
import com.swindlesheets.model.Role;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Contexts {

    private static final Logger logger = LoggerFactory.getLogger(Contexts.class);

    public static AccessToken authenticate(Context context) throws UnauthorizedResponse {
        String authorizationHeader = context.header("Authorization");
        if (authorizationHeader != null) {
            String[] split = authorizationHeader.split(":");
            if (split.length == 3 && split[0].matches("^\\d{1,19}$") &&
                    split[1].matches("^\\d{1,10}$") &&
                    ("true".equals(split[2]) || "false".equals(split[2]))) {
                long timestamp = Long.parseLong(split[0]);
                if (timestamp + 86400000 >= System.currentTimeMillis()) {
                    int userId = Integer.parseInt(split[1]);
                    boolean admin = Boolean.parseBoolean(split[2]);
                    return new AccessToken(userId, Role.USER, timestamp);
                }
            }
        }
        logger.warn("Unauthenticated {} {} request received", context.method(), context.path());
        throw new UnauthorizedResponse("Client must be authenticated to access this resource");
    }

    public static int getResourceId(Context context, String pathParam) {
        String id = context.pathParam(pathParam);
        if (!id.matches("^\\d{1,10}$")) {
            logger.warn("Invalid {} {} request received", context.method(), context.path());
            throw new BadRequestResponse("Invalid URI: Can't parse " + id + " as a resource id");
        }
        return Integer.parseInt(id);
    }

}
