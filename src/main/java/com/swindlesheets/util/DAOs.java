package com.swindlesheets.util;

import com.swindlesheets.dao.CommodityDAO;
import com.swindlesheets.dao.LoadDAO;
import com.swindlesheets.dao.RoutePointDAO;
import com.swindlesheets.dao.UserDAO;

public final class DAOs {

    private static UserDAO userDAO;
    private static LoadDAO loadDAO;
    private static RoutePointDAO routePointDAO;
    private static CommodityDAO commodityDAO;

    public static synchronized UserDAO getUserDAO() {
        if (userDAO == null) { userDAO = new UserDAO(); }
        return userDAO;
    }

    public static synchronized LoadDAO getLoadDAO() {
        if (loadDAO == null) { loadDAO = new LoadDAO(); }
        return loadDAO;
    }

    public static synchronized RoutePointDAO getRoutePointDAO() {
        if (routePointDAO == null) { routePointDAO = new RoutePointDAO(); }
        return routePointDAO;
    }

    public static synchronized CommodityDAO getCommodityDAO() {
        if (commodityDAO == null) { commodityDAO = new CommodityDAO(); }
        return commodityDAO;
    }

}
