package com.swindlesheets.controller;

import com.swindlesheets.service.AuthenticationService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.AccessToken;

public class AuthenticationController {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
    private final AuthenticationService service = new AuthenticationService();

    public void handlePostAuthorize(Context context) {
        String username = context.formParam("username");
        String password = context.formParam("password");
        if (username == null || password == null) {
            logger.warn("Invalid POST /authorize request received");
            context.result("Invalid parameters: username and password parameters are required");
            context.status(400);
        } else {
            logger.info("POST /authorize request received");
            AccessToken token = service.authenticateUser(username, password);
            context.header("Access-Control-Expose-Headers", "Authorization");
            context.header("Authorization", token.toTokenString());
        }
    }

}
