package com.swindlesheets.controller;

import com.swindlesheets.service.AuthenticationService;
import com.swindlesheets.service.UserService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.AccessToken;
import com.swindlesheets.model.User;
import com.swindlesheets.util.Contexts;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final AuthenticationService authService  = new AuthenticationService();
    private final UserService userService = new UserService();

    public void handleGetUser(Context context) {
        int userId = Contexts.getResourceId(context, "userId");
        logger.info("GET /users/{} request received", userId);
        context.json(userService.getUser(userId));
    }

    public void handleGetUsers(Context context) {
        Map<String, String> parameters = parseQueryParamMap(context);
        if (parameters.isEmpty()) {
            logger.info("GET /users request received");
        } else {
            logger.info("GET /users{} request received", context.queryString());
        }
        context.json(userService.queryUsers(parameters));
    }

    private Map<String, String> parseQueryParamMap(Context context) {
        Map<String, String> parameters = new LinkedHashMap<>();
        Map<String, List<String>> queryMap = new LinkedHashMap<>(context.queryParamMap());
        if (queryMap.containsKey("employer")) {
            List<String> employerValues = queryMap.remove("employer");
            if (employerValues.size() != 1) {
                logger.warn("Invalid GET /users{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: employer parameter can have only one value");
            }
            parameters.put("employer", employerValues.get(0));
        }
        if (!queryMap.isEmpty()) {
            logger.warn("Invalid GET /users{} request received", context.queryString());
            String queryParameter = queryMap.keySet().iterator().next();
            throw new BadRequestResponse("Invalid parameters: "
                    + queryParameter + " is not a supported query parameter");
        }
        return parameters;
    }

    public void handlePostUsers(Context context) {
        String username = context.formParam("username");
        String email = context.formParam("email");
        String password = context.formParam("password");
        if (username == null || email == null || password == null) {
            logger.warn("Invalid POST /users request received");
            context.result("Invalid parameters: username, email, and password parameters are required");
            context.status(400);
        } else {
            String employer = context.formParam("employer");
            if (employer == null) { employer = ""; }
            logger.info("POST /users request received");
            userService.createNewUser(username, email, password, employer);
            context.result("New user account for " + username + " has been created");
            context.status(201);
        }
    }

    public void handlePutUser(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int userId = Contexts.getResourceId(context, "userId");
        String password = context.formParam("password");
        if (password != null) {
            logger.info("PUT /users/{} request from User#{}",
                    userId, token.getUserId());
            userService.updateUserPassword(token, userId, password);
            context.result("Password for user " + userId + " has been updated");
        } else {
            User user = getUserFromForm(context);
            if (user == null) {
                logger.warn("Invalid PUT /users/{} request from User#{}",
                        userId, token.getUserId());
                context.result("Invalid parameters: no valid parameters provided to update");
                context.status(400);
            } else {
                //user.setUserId(userId);
                logger.info("Authorized PUT /users/{} request from User#{}",
                        userId, token.getUserId());
                userService.updateUser(token, user);
                context.result("Account for user " + userId + " has been updated");
            }
        }
    }

    private User getUserFromForm(Context context) {
        String username = context.formParam("username");
        String email = context.formParam("email");
        String employer = context.formParam("employer");
        if (username == null && email == null && employer == null) { return null; }
        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setEmployer(employer);
        return user;
    }

    public void handleDeleteUser(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int userId = Contexts.getResourceId(context, "userId");
        logger.info("DELETE /users/{} request from User#{}",
                userId, token.getUserId());
        userService.deleteUser(token, userId);
        context.result("Account for user " + userId + " has been deleted");
    }

}
