package com.swindlesheets.controller;

import com.swindlesheets.model.AccessToken;
import com.swindlesheets.service.RoutePointService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.RoutePoint;
import com.swindlesheets.util.Contexts;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RoutePointController {

    private final Logger logger = LoggerFactory.getLogger(RoutePointController.class);
    private final RoutePointService service = new RoutePointService();

    public void handleGetRoutePoint(Context context) {
        int routePointId = Contexts.getResourceId(context, "routePointId");
        logger.info("GET /route-points/{} request received", routePointId);
        context.json(service.getRoutePoint(routePointId));
    }

    public void handleGetRoutePoints(Context context) {
        Map<String, String> parameters = parseQueryParamMap(context);
        if (parameters.isEmpty()) {
            logger.info("GET /route-points request received");
        } else {
            logger.info("GET /route-points?{} request received", context.queryString());
        }
        context.json(service.queryRoutePoints(parameters));
    }

    private Map<String, String> parseQueryParamMap(Context context) {
        Map<String, String> parameters = new LinkedHashMap<>();
        Map<String, List<String>> queryMap = new LinkedHashMap<>(context.queryParamMap());
        if (queryMap.containsKey("type")) {
            List<String> typeValues = queryMap.remove("type");
            if (typeValues.size() != 1) {
                logger.warn("Invalid GET /route-points{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: " +
                        "type parameter can have only one value");
            }
            parameters.put("type", typeValues.get(0));
        }
        if (queryMap.containsKey("city")) {
            List<String> cityValues = queryMap.remove("city");
            if (cityValues.size() != 1) {
                logger.warn("Invalid GET /route-points{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: " +
                        "city parameter can have only one value");
            }
            parameters.put("city", cityValues.get(0));
        }
        if (queryMap.containsKey("state")) {
            List<String> stateValues = queryMap.remove("state");
            if (stateValues.size() != 1) {
                logger.warn("Invalid GET /route-points{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: " +
                        "state parameter can have only one value");
            }
            parameters.put("state", stateValues.get(0));
        }
        if (queryMap.containsKey("zip")) {
            List<String> zipValues = queryMap.remove("zip");
            if (zipValues.size() != 1) {
                logger.warn("Invalid GET /route-points{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: " +
                        "zip parameter can have only one value");
            }
            parameters.put("zip", zipValues.get(0));
        }
        if (!queryMap.isEmpty()) {
            logger.warn("Invalid GET /route-points{} request received", context.queryString());
            String queryParameter = queryMap.keySet().iterator().next();
            throw new BadRequestResponse("Invalid parameters: "
                    + queryParameter + " is not a supported query parameter");
        }
        return parameters;
    }

    public void handlePostRoutePoints(Context context) {
        AccessToken token = Contexts.authenticate(context);
        RoutePoint routePoint = context.bodyAsClass(RoutePoint.class);
        logger.info("POST /route-points request from User#{}",
                token.getUserId());
        service.createNewRoutePoint(routePoint);
        context.result("New resource in /route-points/ has been created");
        context.status(201);
    }

    public void handlePutRoutePoint(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int routePointId = Contexts.getResourceId(context, "routePointId");
        RoutePoint routePoint = context.bodyAsClass(RoutePoint.class);
        //routePoint.setRoutePointId(routePointId);
        logger.info("PUT /route-points/{} request from User#{}",
                routePointId, token.getUserId());
        service.updateRoutePoint(routePoint);
        context.result("Resource /route-points/" + routePointId + " has been updated");
    }

    public void handleDeleteRoutePoint(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int routePointId = Contexts.getResourceId(context, "routePointId");
        logger.info("DELETE /route-points/{} request from User#{}",
                routePointId, token.getUserId());
        service.deleteRoutePoint(routePointId);
        context.result("Resource /route-points/" + routePointId + " has been deleted");
    }

}
