package com.swindlesheets.controller;

import com.swindlesheets.model.Stop;
import com.swindlesheets.service.LoadService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.AccessToken;
import com.swindlesheets.model.Load;
import com.swindlesheets.util.Contexts;

public class LoadController {

    private final Logger logger = LoggerFactory.getLogger(LoadController.class);
    private final LoadService loadService = new LoadService();

    public void handleGetLoad(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int loadId = Contexts.getResourceId(context, "loadId");
        logger.info("GET /loads/{} request from User#{}",
                loadId, token.getUserId());
        context.json(loadService.getLoad(token, loadId));
    }

    public void handleGetLoads(Context context) {
        /*Map<String, String> parameters = parseQueryParamMap(context);
        if (parameters.isEmpty()) {
            logger.info("GET /loads request received");
        } else {
            logger.info("GET /loads?{} request received", context.queryString());
        }
        context.json(loadService.queryLoads(parameters));
    }

    private Map<String, String> parseQueryParamMap(Context context) {
        Map<String, String> parameters = new LinkedHashMap<>();
        Map<String, List<String>> queryMap = new LinkedHashMap<>(context.queryParamMap());
        if (queryMap.containsKey("user")) {
            List<String> userValues = queryMap.remove("user");
            if (userValues.size() != 1) {
                logger.warn("Invalid GET /loads?{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: user parameter can have only one value");
            }
            String id = userValues.get(0);
            if (!id.matches("^\\d{1,10}$")) {
                logger.warn("Invalid GET /loads?{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: could not parse userId from " + id);
            }
            parameters.put("user", id);
        }
        if (!queryMap.isEmpty()) {
            logger.warn("Invalid GET /loads{} request received", context.queryString());
            String queryParameter = queryMap.keySet().iterator().next();
            throw new BadRequestResponse("Invalid parameters: "
                    + queryParameter + " is not a supported query parameter");
        }
        return parameters;*/
    }

    public void handleGetUserLoads(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int userId = Contexts.getResourceId(context, "userId");
        logger.info("GET user/{}/loads request from User#{}", userId, token.getUserId());
        context.json(loadService.getUserLoads(token, userId));
    }

    public void handlePostLoads(Context context) {
        AccessToken token = Contexts.authenticate(context);
        Load load = context.bodyAsClass(Load.class);
        logger.info("POST /loads request from User#{}", token.getUserId());
        loadService.saveNewLoad(token, load);
        context.json(load);
        context.status(201);
    }

    public void handlePutLoad(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int loadId = Contexts.getResourceId(context, "loadId");
        Load load = context.bodyAsClass(Load.class);
        logger.info("PUT /loads/{} request from User#{}",
                loadId, token.getUserId());
        loadService.updateLoad(token, load);
        context.result("Resource /loads/" + loadId + " has been updated");
    }

    public void handleDeleteLoad(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int loadId = Contexts.getResourceId(context, "loadId");
        logger.info("DELETE /loads/{} request from User#{}",
                loadId, token.getUserId());
        loadService.deleteLoad(token, loadId);
        context.result("Resource /loads/" + loadId + " has been deleted");
    }

    public void handlePostStop(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int loadId = Contexts.getResourceId(context, "loadId");
        Stop stop = context.bodyAsClass(Stop.class);
        logger.info("POST /loads/{}/stops request from User#{}",
                loadId, token.getUserId());
        loadService.saveNewStop(token, loadId, stop);
        context.status(201);
    }

    public void handlePutStop(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int loadId = Contexts.getResourceId(context, "loadId");
        int stopId = Contexts.getResourceId(context, "stopId");
        logger.info("PUT /loads/{}/stops/{} request from User#{}",
                loadId, stopId, token.getUserId());
        //loadService.updateStop(token, loadId, stopId);
    }

    public void handleDeleteStop(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int loadId = Contexts.getResourceId(context, "loadId");
        int stopId = Contexts.getResourceId(context, "stopId");
        logger.info("DELETE /loads/{}/stops/{} request from User#{}",
                loadId, stopId, token.getUserId());
        loadService.deleteStop(token, loadId, stopId);
    }

}
