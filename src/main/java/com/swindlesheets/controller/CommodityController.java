package com.swindlesheets.controller;

import com.swindlesheets.service.CommodityService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.swindlesheets.model.AccessToken;
import com.swindlesheets.model.Commodity;
import com.swindlesheets.util.Contexts;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CommodityController {

    private final Logger logger = LoggerFactory.getLogger(CommodityController.class);
    private final CommodityService service = new CommodityService();

    public void handleGetCommodity(Context context) {
        int commodityId = Contexts.getResourceId(context, "commodityId");
        logger.info("GET /commodities/{} request received", commodityId);
        context.json(service.getCommodity(commodityId));
    }

    public void handleGetCommodities(Context context) {
        Map<String, String> parameters = parseQueryParamMap(context);
        if (parameters.isEmpty()) {
            logger.info("GET /commodities request received");
        } else {
            logger.info("GET /commodities?{} request received", context.queryString());
        }
        context.json(service.queryCommodities(parameters));
    }

    private Map<String, String> parseQueryParamMap(Context context) {
        Map<String, String> parameters = new LinkedHashMap<>();
        Map<String, List<String>> queryMap = new LinkedHashMap<>(context.queryParamMap());
        if (queryMap.containsKey("description")) {
            List<String> descriptionValues = queryMap.remove("description");
            if (descriptionValues.size() != 1) {
                logger.warn("Invalid GET /commodities?{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: " +
                        "description parameter can have only one value");
            }
            parameters.put("description", descriptionValues.get(0));
        }
        if (queryMap.containsKey("placard")) {
            List<String> placardValues = queryMap.remove("placard");
            if (placardValues.size() != 1) {
                logger.warn("Invalid GET /commodities?{} request received", context.queryString());
                throw new BadRequestResponse("Invalid parameters: " +
                        "placard parameter can have only one value");
            }
            parameters.put("placard", placardValues.get(0));
        }
        if (!queryMap.isEmpty()) {
            logger.warn("Invalid GET /commodities{} request received", context.queryString());
            String queryParameter = queryMap.keySet().iterator().next();
            throw new BadRequestResponse("Invalid parameters: "
                    + queryParameter + " is not a supported query parameter");
        }
        return parameters;
    }

    public void handlePostCommodities(Context context) {
        AccessToken token = Contexts.authenticate(context);
        Commodity commodity = context.bodyAsClass(Commodity.class);
        logger.info("POST /commodities request from User#{}", token.getUserId());
        service.createNewCommodity(commodity);
        context.result("New resource in /commodities/ has been created");
        context.status(201);
    }

    public void handlePutCommodity(Context context) {
        /*AccessToken token = authenticate(context);
        authorize(context, token);
        int commodityId = getResourceId(context);
        Commodity commodity = context.bodyAsClass(Commodity.class);
        commodity.setCommodityId(commodityId);
        logger.info("Authorized PUT /commodities/{} request received from user {}",
                commodityId, token.getUserId());
        service.updateCommodity(commodity);
        context.result("Resource /commodities/" + commodityId + " has been updated");*/
    }

    public void handleDeleteCommodity(Context context) {
        AccessToken token = Contexts.authenticate(context);
        int commodityId = Contexts.getResourceId(context, "commodityId");
        logger.info("DELETE /commodities/{} request received from User#{}",
                commodityId, token.getUserId());
        service.deleteCommodity(commodityId);
        context.result("Resource /commodities/" + commodityId + " has been deleted");
    }

}
