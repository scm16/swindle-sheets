package com.swindlesheets.controller;

public class RoutePointControllerIntegrationTest {
/*
    private static JavalinApplication app = new JavalinApplication();

    @BeforeAll
    public static void start() {
        app.start(7000);
    }

    @AfterAll
    public static void stop() {
        app.stop();
    }

    @Test
    public void testGetRoutePointInvalidId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/route-points/invalid")
                .asString();
        String expected = "Invalid URI: Can't parse invalid as a resource id";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetRoutePointNonexistentId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/route-points/2000000000")
                .asString();
        String expected = "Could not locate /route-points/2000000000";
        assertAll(
                () -> assertEquals(404, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetRoutePointExistentId() {
        HttpResponse<RoutePoint> response = Unirest.get("http://localhost:7000/route-points/1")
                .asObject(RoutePoint.class);
        RoutePoint routePoint = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertEquals(1, routePoint.getRoutePointId())
        );
    }

    @Test
    public void testGetRoutePointsInvalidQueryParameterKey() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/route-points?invalid=1")
                .asString();
        String expected = "Invalid parameters: invalid is not a supported query parameter";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetRoutePointsMultipleQueryParameterValues() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/route-points?type=SHIPPER&type=RECEIVER")
                .asString();
        String expected = "Invalid parameters: type parameter can have only one value";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetRoutePointsInvalidQueryParameterValue() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/route-points?type=invalid")
                .asString();
        String expected = "Invalid parameters: type must be \"SHIPPER\" or \"RECEIVER\"";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetRoutePointsValidQueryParameters() {
        HttpResponse<List<RoutePoint>> response = Unirest.get("http://localhost:7000/route-points?type=SHIPPER&state=DC")
                .asObject(new GenericType<List<RoutePoint>>() {});
        List<RoutePoint> routePoints = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertFalse(routePoints.isEmpty())
        );
    }

    @Test
    public void testGetRoutePointsNoQueryParameters() {
        HttpResponse<List<RoutePoint>> response = Unirest.get("http://localhost:7000/route-points")
                .asObject(new GenericType<List<RoutePoint>>() {});
        List<RoutePoint> routePoints = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertFalse(routePoints.isEmpty())
        );
    }

    @Test
    public void testPostRoutePointsUnauthenticated() {
        RoutePoint routePoint = new RoutePoint();
        routePoint.setType("RECEIVER");
        routePoint.setName("some-name");
        routePoint.setAddress("123 abc");
        routePoint.setCity("some-city");
        routePoint.setState("VA");
        routePoint.setZip("12345");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/route-points")
                .body(routePoint)
                .asString();
        String expected = "Client must be authenticated to access this resource";
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostRoutePointsUnauthorized() {
        String tokenString = System.currentTimeMillis() + ":2:false";
        RoutePoint routePoint = new RoutePoint();
        routePoint.setType("RECEIVER");
        routePoint.setName("some-name");
        routePoint.setAddress("123 abc");
        routePoint.setCity("some-city");
        routePoint.setState("VA");
        routePoint.setZip("12345");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/route-points")
                .header("Authorization", tokenString)
                .body(routePoint)
                .asString();
        String expected = "Client is not authorized to access this resource in this manner";
        assertAll(
                () -> assertEquals(403, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testDeleteRoutePointUnauthenticated() {
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/route-points/1")
                .asString();
        String expected = "Client must be authenticated to access this resource";
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testDeleteRoutePointUnauthorized() {
        String tokenString = System.currentTimeMillis() + ":2:false";
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/route-points/1")
                .header("Authorization", tokenString)
                .asString();
        String expected = "Client is not authorized to access this resource in this manner";
        assertAll(
                () -> assertEquals(403, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }
*/
}
