package com.swindlesheets.controller;

public class CommodityControllerIntegrationTest {
/*
    private static JavalinApplication app = new JavalinApplication();

    @BeforeAll
    public static void start() {
        app.start(7000);
    }

    @AfterAll
    public static void stop() {
        app.stop();
    }

    @Test
    public void testGetCommodityInvalidId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/commodities/invalid")
                .asString();
        String expected = "Invalid URI: Can't parse invalid as a resource id";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetCommodityNonexistentId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/commodities/2000000000")
                .asString();
        String expected = "Could not locate /commodities/2000000000";
        assertAll(
                () -> assertEquals(404, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetCommodityExistentId() {
        HttpResponse<Commodity> response = Unirest.get("http://localhost:7000/commodities/1")
                .asObject(Commodity.class);
        Commodity commodity = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertEquals(1, commodity.getCommodityId())
        );
    }

    @Test
    public void testGetCommoditiesInvalidQueryParameterKey() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/commodities?invalid=1")
                .asString();
        String expected = "Invalid parameters: invalid is not a supported query parameter";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetCommoditiesMultipleQueryParameterValues() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/commodities?placard=a&placard=b")
                .asString();
        String expected = "Invalid parameters: placard parameter can have only one value";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetCommoditiesInvalidQueryParameterValue() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/commodities?placard=invalid")
                .asString();
        String expected = "Invalid parameters: nonexistent placard provided";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetCommoditiesValidQueryParameters() {
        HttpResponse<List<Commodity>> response = Unirest.get("http://localhost:7000/commodities?placard=GAS")
                .asObject(new GenericType<List<Commodity>>() {});
        List<Commodity> commodities = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertFalse(commodities.isEmpty())
        );
    }

    @Test
    public void testGetCommoditiesNoQueryParameters() {
        HttpResponse<List<Commodity>> response = Unirest.get("http://localhost:7000/commodities")
                .asObject(new GenericType<List<Commodity>>() {});
        List<Commodity> commodities = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertFalse(commodities.isEmpty())
        );
    }

    @Test
    public void testPostCommoditiesUnauthenticated() {
        Commodity commodity = new Commodity();
        commodity.setDescription("new-commodity");
        commodity.setPlacard("NONE");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/commodities")
                .body(commodity)
                .asString();
        String expected = "Client must be authenticated to access this resource";
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostCommoditiesUnauthorized() {
        String tokenString = System.currentTimeMillis() + ":2:false";
        Commodity commodity = new Commodity();
        commodity.setDescription("new-commodity");
        commodity.setPlacard("NONE");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/commodities")
                .header("Authorization", tokenString)
                .body(commodity)
                .asString();
        String expected = "Client is not authorized to access this resource in this manner";
        assertAll(
                () -> assertEquals(403, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostCommoditiesInvalidDescriptionParameter() {
        String tokenString = System.currentTimeMillis() + ":1:true";
        Commodity commodity = new Commodity();
        commodity.setDescription("0123456789abcdef0123456789abcdef123456789");
        commodity.setPlacard("NONE");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/commodities")
                .header("Authorization", tokenString)
                .body(commodity)
                .asString();
        String expected = "Invalid parameters: description must not be empty and have 40 or less characters";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostCommoditiesInvalidPlacardParameter() {
        String tokenString = System.currentTimeMillis() + ":1:true";
        Commodity commodity = new Commodity();
        commodity.setDescription("new-commodity");
        commodity.setPlacard("invalid");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/commodities")
                .header("Authorization", tokenString)
                .body(commodity)
                .asString();
        String expected = "Invalid parameters: nonexistent placard provided";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostCommoditiesIdenticalConflict() {
        String tokenString = System.currentTimeMillis() + ":1:true";
        Commodity commodity = new Commodity();
        commodity.setDescription("Uranium");
        commodity.setPlacard("RADIOACTIVE");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/commodities")
                .header("Authorization", tokenString)
                .body(commodity)
                .asString();
        String expected = "Uranium already exists";
        assertAll(
                () -> assertEquals(409, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testDeleteCommodityUnauthenticated() {
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/commodities/1")
                .asString();
        String expected = "Client must be authenticated to access this resource";
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testDeleteCommodityUnauthorized() {
        String tokenString = System.currentTimeMillis() + ":2:false";
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/commodities/1")
                .header("Authorization", tokenString)
                .asString();
        String expected = "Client is not authorized to access this resource in this manner";
        assertAll(
                () -> assertEquals(403, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }
*/
}
