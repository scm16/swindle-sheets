package com.swindlesheets.controller;

public class LoadControllerIntegrationTest {
/*
    private static JavalinApplication app = new JavalinApplication();

    @BeforeAll
    public static void start() {
        app.start(7000);
    }

    @AfterAll
    public static void stop() {
        app.stop();
    }

    @Test
    public void testGetLoadInvalidId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/loads/invalid")
                .asString();
        String expected = "Invalid URI: Can't parse invalid as a resource id";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetLoadNonexistentId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/loads/2000000000")
                .asString();
        String expected = "Could not locate /loads/2000000000";
        assertAll(
                () -> assertEquals(404, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetLoadExistentId() {
        HttpResponse<Load> response = Unirest.get("http://localhost:7000/loads/1")
                .asObject(Load.class);
        Load load = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertEquals(1, load.getLoadId())
        );
    }

    @Test
    public void testGetLoadsInvalidQueryParameterKey() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/loads?invalid=1")
                .asString();
        String expected = "Invalid parameters: invalid is not a supported query parameter";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetLoadsMultipleQueryParameterValues() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/loads?user=2&user=2")
                .asString();
        String expected = "Invalid parameters: user parameter can have only one value";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetLoadsValidQueryParameters() {
        HttpResponse<List<Load>> response = Unirest.get("http://localhost:7000/loads?user=2")
                .asObject(new GenericType<List<Load>>() {});
        List<Load> loads = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertFalse(loads.isEmpty())
        );
    }

    @Test
    public void testGetLoadsNoQueryParameters() {
        HttpResponse<List<Load>> response = Unirest.get("http://localhost:7000/loads")
                .asObject(new GenericType<List<Load>>() {});
        List<Load> loads = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertFalse(loads.isEmpty())
        );
    }

    @Test
    public void testPostLoadsUnauthenticated() {
        Load load = new Load();
        HttpResponse<String> response = Unirest.post("http://localhost:7000/loads")
                .body(load)
                .asString();
        String expected = "Client must be authenticated to access this resource";
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostLoadsInvalidNotesParameter() {
        String tokenString = System.currentTimeMillis() + ":2:false";
        Load load = new Load();
        load.setUserId(2);
        load.setNotes("0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789abcdef");
        HttpResponse<String> response = Unirest.post("http://localhost:7000/loads")
                .header("Authorization", tokenString)
                .body(load)
                .asString();
        String expected = "Invalid parameters: notes must be within 100 characters";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testDeleteLoadUnauthenticated() {
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/loads/1")
                .asString();
        String expected = "Client must be authenticated to access this resource";
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }
*/
}
