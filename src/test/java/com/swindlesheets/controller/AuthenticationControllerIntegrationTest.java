package com.swindlesheets.controller;

public class AuthenticationControllerIntegrationTest {
/*
    private static JavalinApplication app = new JavalinApplication();

    @BeforeAll
    public static void start() {
        app.start(7000);
    }

    @AfterAll
    public static void stop() {
        app.stop();
    }

    @Test
    public void testPostAuthorizeMissingUsername() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/authorize")
                .field("password", "password")
                .asString();
        String expected = "Invalid parameters: username and password parameters are required";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostAuthorizeMissingPassword() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/authorize")
                .field("username", "user")
                .asString();
        String expected = "Invalid parameters: username and password parameters are required";
        assertAll(
                ()->assertEquals(400, response.getStatus()),
                ()->assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostAuthorizeUnregisteredUsername() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/authorize")
                .field("username", "unregistered-user")
                .field("password", "password")
                .asString();
        String expected = "Could not authenticate from provided credentials";
        assertAll(
                ()->assertEquals(401, response.getStatus()),
                ()->assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostAuthorizeRegisteredUsernameWrongPassword() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/authorize")
                .field("username", "user")
                .field("password", "wrong-password")
                .asString();
        String expected = "Could not authenticate from provided credentials";
        assertAll(
                ()->assertEquals(401, response.getStatus()),
                ()->assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostAuthorizeRegisteredUsernameCorrectPassword() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/authorize")
                .field("username", "user")
                .field("password", "password")
                .asString();
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().matches(
                        "^Access Token for authentication: \\d{1,19}:\\d{1,10}:(true|false)$"))
        );
    }
*/
}
