package com.swindlesheets.controller;

public class UserControllerIntegrationTest {
/*
    private static final JavalinApplication app = new JavalinApplication();

    @BeforeAll
    public static void start() {
        app.start(7000);
    }

    @AfterAll
    public static void stop() {
        app.stop();
    }

    @Test
    public void testGetUserInvalidId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/users/invalid")
                .asString();
        String expected = "Invalid URI: Can't parse invalid as a resource id";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetUserNonexistentId() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/users/2000000000")
                .asString();
        String expected = "Could not locate /users/2000000000";
        assertAll(
                () -> assertEquals(404, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetUserExistentId() {
        HttpResponse<User> response = Unirest.get("http://localhost:7000/users/1")
                .asObject(User.class);
        User user = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertEquals(1, user.getUserId())
        );
    }

    @Test
    public void testGetUsersInvalidQueryParameterKey() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/users?invalid=1")
                .asString();
        String expected = "Invalid parameters: invalid is not a supported query parameter";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetUsersMultipleQueryParameterValues() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/users?employer=a&employer=b")
                .asString();
        String expected = "Invalid parameters: employer parameter can have only one value";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetUsersInvalidQueryParameterValue() {
        HttpResponse<String> response = Unirest.get("http://localhost:7000/users?employer=" +
                "0123456789abcdef0123456789abcdef123456789")
                .asString();
        String expected = "Invalid parameters: employer must be within 40 characters";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testGetUsersValidQueryParameters() {
        HttpResponse<List<User>> response = Unirest.get("http://localhost:7000/users?employer=Example")
                .asObject(new GenericType<List<User>>() {});
        List<User> users = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertTrue(!users.isEmpty())
        );
    }

    @Test
    public void testGetUsersNoQueryParameters() {
        HttpResponse<List<User>> response = Unirest.get("http://localhost:7000/users")
                .asObject(new GenericType<List<User>>() {});
        List<User> users = response.getBody();
        assertAll(
                () -> assertEquals(200, response.getStatus()),
                () -> assertTrue(!users.isEmpty())
        );
    }

    @Test
    public void testPostUsersMissingRequiredParameters() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users")
                .asString();
        String expected = "Invalid parameters: username, email, and password parameters are required";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostUsersInvalidUsernameParameter() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users")
                .field("username", "0123456789abcdef0123456789abcdef123456789")
                .field("email", "email@email.com")
                .field("password", "password")
                .asString();
        String expected = "Invalid parameters: username must not be empty and be within 40 characters";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostUsersInvalidEmailParameter() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users")
                .field("username", "new-user")
                .field("email", "0123456789abcdef0123456789@abcdef123456789.com")
                .field("password", "password")
                .asString();
        String expected = "Invalid parameters: email must not be empty and be within 40 characters";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostUsersInvalidPasswordParameter() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users")
                .field("username", "new-user")
                .field("email", "email@email.com")
                .field("password", "")
                .asString();
        String expected = "Invalid parameters: password must not be empty";
        assertAll(
                () -> assertEquals(400, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostUsersTakenUsernameParameter() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users")
                .field("username", "admin")
                .field("email", "email@email.com")
                .field("password", "password")
                .asString();
        String expected = "admin is already taken as a username";
        assertAll(
                () -> assertEquals(409, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testPostUsersTakenEmailParameter() {
        HttpResponse<String> response = Unirest.post("http://localhost:7000/users")
                .field("username", "new-user")
                .field("email", "admin@truckeraid.com")
                .field("password", "password")
                .asString();
        String expected = "admin@truckeraid.com is already taken as an email";
        assertAll(
                () -> assertEquals(409, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testDeleteUserUnauthenticated() {
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/users/1")
                .asString();
        String expected = "Client must be authenticated to access this resource";
        assertAll(
                () -> assertEquals(401, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }

    @Test
    public void testDeleteUserUnauthorized() {
        String tokenString = System.currentTimeMillis() + ":2:false";
        HttpResponse<String> response = Unirest.delete("http://localhost:7000/users/1")
                .header("Authorization", tokenString)
                .asString();
        String expected = "Client is not authorized to access this resource in this manner";
        assertAll(
                () -> assertEquals(403, response.getStatus()),
                () -> assertEquals(expected, response.getBody())
        );
    }
*/
}
